package org.dnyanyog.dto.request;

import org.springframework.stereotype.Component;

@Component
public class LoginRequest {
	private String mobileNo;
	private String passcode;
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getPasscode() {
		return passcode;
	}
	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}

}
