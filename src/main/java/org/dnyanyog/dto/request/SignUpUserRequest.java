package org.dnyanyog.dto.request;

import org.springframework.stereotype.Component;

@Component
public class SignUpUserRequest {
	private long grnNo;
	private String mobileNo;
	private String email;
	public long getGrnNo() {
		return grnNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setGrnNo(long grnNo) {
		this.grnNo = grnNo;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public String getPasscode() {
		return passcode;
	}
	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}
	public String getConfirmPasscode() {
		return confirmPasscode;
	}
	public void setConfirmPasscode(String confirmPasscode) {
		this.confirmPasscode = confirmPasscode;
	}
	private String userRole;
	private String passcode;
	private String confirmPasscode;

}
