package org.dnyanyog.dto.request;

import java.time.LocalDate;

import org.springframework.stereotype.Component;
@Component
public class DiaryRequest {

	private Long grnNO;
	private String studentName;
	private String information;
	private LocalDate date;
	
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getInformation() {
		return information;
	}
	public void setInformation(String information) {
		this.information = information;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public Long getGrnNO() {
		return grnNO;
	}
	public void setGrnNO(Long grnNO) {
		this.grnNO = grnNO;
	}
	
	
}
