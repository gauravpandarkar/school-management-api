package org.dnyanyog.dto.request;

import org.springframework.stereotype.Component;

@Component
public class AttendenceRequest {
	private String rollNo;
	private String name;
	private String curDate;
	public String getCurDate() {
		return curDate;
	}
	public void setCurDate(String date) {
		this.curDate = date;
	}
	public String getRollNo() {
		return rollNo;
	}
	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAttendance() {
		return attendancne;
	}
	public void setAttendance(String attendacne) {
		this.attendancne = attendacne;
	}
	private String attendancne;

}
