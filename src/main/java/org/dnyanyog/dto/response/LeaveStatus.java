package org.dnyanyog.dto.response;

import org.springframework.stereotype.Component;

@Component
public class LeaveStatus {
	private LeaveStatus status;

	public LeaveStatus getStatus() {
		return status;
	}

	public void setStatus(LeaveStatus  status) {
		this.status = status;
	}



}
