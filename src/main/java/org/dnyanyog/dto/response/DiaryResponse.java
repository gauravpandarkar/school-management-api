package org.dnyanyog.dto.response;

import org.springframework.stereotype.Component;

@Component
public class DiaryResponse {

	private String Status;
	private String message;
	private DiaryData Data;
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public DiaryData getData() {
		return Data;
	}
	public void setData(DiaryData data) {
		Data = data;
	}
	
	
}
