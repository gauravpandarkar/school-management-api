package org.dnyanyog.dto.response;

import org.springframework.stereotype.Component;

@Component
public class FeesResponse {
	private String Status;
	private String Message;
	private FeesData Data;
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public FeesData getData() {
		return Data;
	}
	public void setData(FeesData data) {
		Data = data;
	}
	
	
	


}
