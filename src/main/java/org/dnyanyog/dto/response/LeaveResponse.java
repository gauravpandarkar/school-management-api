package org.dnyanyog.dto.response;

import org.springframework.stereotype.Component;

@Component
public class LeaveResponse {
	private String Status;
	 private String Message;
	 private LeaveData Data;
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public LeaveData getData() {
		return Data;
	}
	public void setData(LeaveData data) {
		Data = data;
	}
	 


}
