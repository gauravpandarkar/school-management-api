package org.dnyanyog.dto.response;

import org.springframework.stereotype.Component;

@Component
public class OtpResponse {
	private String to;
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	private String otp;

}
