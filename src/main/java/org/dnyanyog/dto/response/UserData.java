package org.dnyanyog.dto.response;

import org.springframework.stereotype.Component;

@Component
public class UserData {
	private long grnNo;
	private String mobileNo;
	
	private String userRole;
	private String email;
	public String getMobileNo() {
		return mobileNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getGrnNo() {
		return grnNo;
	}
	public void setGrnNo(long grnNo) {
		this.grnNo = grnNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

}
