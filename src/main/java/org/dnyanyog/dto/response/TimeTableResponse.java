package org.dnyanyog.dto.response;

import org.springframework.stereotype.Component;

@Component
public class TimeTableResponse {
	private String Status;
	private String message;
	private TimeTableData Data;
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public TimeTableData getData() {
		return Data;
	}
	public void setData(TimeTableData data) {
		Data = data;
	}



}
