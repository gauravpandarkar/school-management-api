package org.dnyanyog.dto.response;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class ExamResponse {
 
	private String Status;
	private String Message;
	private ExamData Data;
	
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public ExamData getData() {
		return Data;
	}
	public void setData(ExamData data) {
		Data = data;
	}
	
	
	
}

