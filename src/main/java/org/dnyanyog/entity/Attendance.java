package org.dnyanyog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Table
@Entity
@Component
public class Attendance {
	@Id
	@GeneratedValue
	private long rollNo;
	@Column
	private String name;

	@Column
	private String attendance;
	@Column
	private String curDate;


	public long getRollNo() {
		return rollNo;
	}

	public void setRollNo(long rollNo) {
		this.rollNo = rollNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAttendance() {
		return attendance;
	}

	public String getCurDate() {
		return curDate;
	}

	public void setCurDate(String date) {
		curDate = date;
	}

	public void setAttendance(String attendance) {
		this.attendance = attendance;
	}

	
}
