package org.dnyanyog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

@Table
@Entity
@Component
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long grnNo;
	
	@NotEmpty(message = "Studentfirstname is required")
	@Size(min = 3, max = 16, message = "Studentfirstname must be between 3 and 16 characters")
	@Pattern(regexp = "^[a-zA-Z]+(?:-[a-zA-Z]+)*$", message = "Studentfirst name must contain only letters and hyphens")
	@Column
	private String studentFirstName;
	@NotEmpty(message = "Fatherfirstname is required")
	@Size(min = 3, max = 16, message = "Fatherfirstname must be between 3 and 16 characters")
	@Pattern(regexp = "^[a-zA-Z]+(?:-[a-zA-Z]+)*$", message = "Fatherfirstname must contain only letters and hyphens")
	@Column
	private String fatherFirstName;
	@NotEmpty(message = "motherFirstName is required")
	@Size(min = 3, max = 16, message = "motherFirstName must be between 3 and 16 characters")
	@Pattern(regexp = "^[a-zA-Z]+(?:-[a-zA-Z]+)*$", message = "motherFirstName must contain only letters and hyphens")
	@Column
	private String motherFirstName;
	@NotEmpty(message = "lastName is required")
	@Size(min = 3, max = 16, message = "lastName must be between 3 and 16 characters")
	@Pattern(regexp = "^[a-zA-Z]+(?:-[a-zA-Z]+)*$", message = "lastName must contain only letters and hyphens")
	@Column
	private String lastName;
	@NotEmpty(message = "Mobile number is required")
	@Pattern(regexp = "^\\d{10}$", message = "Mobile number must be a 10-digit number")
	@Column(unique = true)
	private String parentMobileNo;
	@NotEmpty(message = "Address is required")
	@Pattern(regexp = "^[a-zA-Z0-9\\s,-]+$", message = "Address must contain only letters, numbers, spaces, commas, and hyphens")
	@Column
	private String address;
	//@NotEmpty(message = "Birth date is required")
	//@Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$", message = "Birth date must be in the format YYYY-MM-DD")
	@Column
	private String studentBirthDate;
	@NotEmpty(message = "Gender is required")
	@Pattern(regexp = "^(Male|Female|Other)$", message = "Gender must be Male, Female, or Other")
	@Column
	private String gender;
	@Column
	private String classToApply;
	@NotEmpty(message = "School name is required")
	@Size(min = 3, max = 50, message = "School name must be between 3 and 50 characters")
	@Column
	private String previousSchoolName;
	@NotEmpty(message = "Mobile number is required")
	@Pattern(regexp = "^\\d{10}$", message = "Mobile number must be a 10-digit number")
	@Column
	private String studentMobileNo;
	@NotEmpty(message = "Email is required")
	@Email(message = "Invalid email format")
	@Column
	private String studentEmail;

	

	public long getGrnNo() {
		return grnNo;
	}

	public void setGrnNo(long grnNo) {
		this.grnNo = grnNo;
	}

	public String getStudentFirstName() {
		return studentFirstName;
	}

	public void setStudentFirstName(String studentFirstName) {
		this.studentFirstName = studentFirstName;
	}

	public String getFatherFirstName() {
		return fatherFirstName;
	}

	public void setFatherFirstName(String fatherFirstName) {
		this.fatherFirstName = fatherFirstName;
	}

	public String getMotherFirstName() {
		return motherFirstName;
	}

	public void setMotherFirstName(String motherFirstName) {
		this.motherFirstName = motherFirstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getParentMobileNo() {
		return parentMobileNo;
	}

	public void setParentMobileNo(String parentMobileNo) {
		this.parentMobileNo = parentMobileNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStudentBirthDate() {
		return studentBirthDate;
	}

	public void setStudentBirthDate(String studentBirthDate) {
		this.studentBirthDate = studentBirthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getClassToApply() {
		return classToApply;
	}

	public void setClassToApply(String classToApply) {
		this.classToApply = classToApply;
	}

	public String getPreviousSchoolName() {
		return previousSchoolName;
	}

	public void setPreviousSchoolName(String previousSchoolName) {
		this.previousSchoolName = previousSchoolName;
	}

	public String getStudentMobileNo() {
		return studentMobileNo;
	}

	public void setStudentMobileNo(String studentMobileNo) {
		this.studentMobileNo = studentMobileNo;
	}

	public String getStudentEmail() {
		return studentEmail;
	}

	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}

}
