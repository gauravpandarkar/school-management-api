package org.dnyanyog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Table
@Entity
@Component
public class FeesInfo {
	 @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private long id;

	    @Column
	    private long grnNo;

	    @Column
	    private String studentName;

	    @Column
	    private String appliedClass;

	    @Column
	    private String totalFees;

	    @Column
	    private String feesPaid;

	    public String getRemainingFees() {
			return remainingFees;
		}

		public void setRemainingFees(String remainingFees) {
			this.remainingFees = remainingFees;
		}

		@Column
	    private String remainingFees;

	    @Column
	    private String acadmicYear;

	    // Default constructor (required by JPA)
	    public FeesInfo() {
	    }

	    // Constructor with parameters
	    public FeesInfo(long grnNo, String studentName, String appliedClass, long id, String totalFees, String feesPaid,
	            String acadmicYear, String remainingFees) {
	        // Initialize the instance variables with the provided arguments
	        this.grnNo = grnNo;
	        this.studentName = studentName;
	        this.appliedClass = appliedClass;
	        this.id = id;
	        this.totalFees = totalFees;
	        this.feesPaid = feesPaid;
	        this.acadmicYear = acadmicYear;
	        this.remainingFees = remainingFees;
	    }

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public long getGrnNo() {
			return grnNo;
		}

		public void setGrnNo(long grnNo) {
			this.grnNo = grnNo;
		}

		public String getStudentName() {
			return studentName;
		}

		public void setStudentName(String studentName) {
			this.studentName = studentName;
		}

		public String getAppliedClass() {
			return appliedClass;
		}

		public void setAppliedClass(String appliedClass) {
			this.appliedClass = appliedClass;
		}

		public String getTotalFees() {
			return totalFees;
		}

		public void setTotalFees(String totalFees) {
			this.totalFees = totalFees;
		}

		public String getFeesPaid() {
			return feesPaid;
		}

		public void setFeesPaid(String feesPaid) {
			this.feesPaid = feesPaid;
		}

	

		public String getAcadmicYear() {
			return acadmicYear;
		}

		public void setAcadmicYear(String acadmicYear) {
			this.acadmicYear = acadmicYear;
		}

	 


}
