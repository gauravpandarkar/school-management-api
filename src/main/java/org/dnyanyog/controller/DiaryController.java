package org.dnyanyog.controller;

import java.util.List;

import org.dnyanyog.dto.request.DiaryRequest;
import org.dnyanyog.dto.response.DiaryResponse;
import org.dnyanyog.entity.DiaryInfo;
import org.dnyanyog.service.DiaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class DiaryController {

	@Autowired
	DiaryService diaryservice;
	
	
	@PostMapping(path="/Diary/Api/Add")
	public ResponseEntity<DiaryResponse>addInformation(@RequestBody DiaryRequest request){
		
		return diaryservice.addInfo(request);
	}
	
	@GetMapping(path="/Diary/Api/Find/{grnNO}")
	public List<DiaryInfo>getInformation(@PathVariable Long grnNO){
		
		return diaryservice.findbyGrnNO(grnNO);
	}
	
	@PutMapping(path="/Diary/Api/Update/{id}")
	public ResponseEntity<DiaryInfo>updateDiaryInfo(@PathVariable Long id, @RequestBody DiaryInfo updateDiaryInfo){
		
		DiaryInfo UpdatedDiaryInfo=diaryservice.updateDiaryInfo(id, updateDiaryInfo);
		
		if(UpdatedDiaryInfo!=null) {
			return ResponseEntity.ok(UpdatedDiaryInfo);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
		
	@DeleteMapping(path="/Diary/Api/Delete/{id}")
	public ResponseEntity<String>deleteDiaryInfo(@PathVariable Long id){
	boolean	 Delete= diaryservice.deleteDiaryInfoById(id);
	
	if(Delete) {
		return ResponseEntity.ok("Diary with ID " + id + " deleted successfully");
	}else {
		return ResponseEntity.notFound().build();
	}
	
	}
}
