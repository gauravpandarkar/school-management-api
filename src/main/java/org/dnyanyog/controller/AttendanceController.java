package org.dnyanyog.controller;

import org.dnyanyog.dto.request.AttendenceRequest;
import org.dnyanyog.dto.response.AttendanceResponse;
import org.dnyanyog.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AttendanceController {
	@Autowired
	AttendanceService attendanceService;
	
	
	@PostMapping(path="api/school/attendance", produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE},consumes= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<AttendanceResponse> signUp(@RequestBody AttendenceRequest request){
		return attendanceService.saveData(request);
	}
	

}
