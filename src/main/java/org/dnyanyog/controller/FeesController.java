package org.dnyanyog.controller;

import java.util.List;

import org.dnyanyog.dto.request.FeesRequest;
import org.dnyanyog.dto.response.FeesResponse;
import org.dnyanyog.entity.FeesInfo;
import org.dnyanyog.service.FeesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeesController {
	@Autowired
	FeesService feesService;

	@PostMapping(path="Fees/api/add")
	public ResponseEntity<FeesResponse> saveData(@RequestBody FeesRequest feesRequest) {
		return feesService.saveData(feesRequest);
	}
	
//	 @GetMapping(path="/Fees/api/v1/{attribute}")
//	    public ResponseEntity<?> getFeesInfoByAttribute(@PathVariable("attribute") String attributeValue) {
//	        List<FeesInfo> feesInfoList = feesService.getFeesInfoByAttribute(attributeValue);
//	        if (!feesInfoList.isEmpty()) {
//	            return ResponseEntity.ok(feesInfoList);
//	        } else {
//	            return ResponseEntity.notFound().build();
//	        }
//	    }
	@GetMapping(path = "/fees/api/{grnNo}")
	public List<FeesInfo> getFeesInfo(@PathVariable long grnNo) {
		return feesService.getFeesInfo(grnNo);

	}

//	 @GetMapping(path="/fees/api/{id}")
//	    public List<FeesInfo> getFeesInfo(@PathVariable long id) {
//		 return feesService.getFeesInfo(id);
//	        
//	 } 
	 @DeleteMapping("/Fees/api/delete/{grnNo}")
	    public ResponseEntity<?> deleteFeesInfoByGrnNo(@PathVariable("grnNo") long grnNo) {
	        boolean deleted = feesService.deleteFeesInfoByGrnNo(grnNo);
	        if (deleted) {
	            return ResponseEntity.ok().build();
	        } else {
	            return ResponseEntity.notFound().build();
	        }
	    }
	 @PutMapping("/Fees/api/update/{id}")
		public ResponseEntity<FeesInfo> updateFeesInfo(@PathVariable Long id, @RequestBody FeesInfo updatedFeesInfo) {
			FeesInfo updatedFeesInfoRecord = feesService.updateFeesInfo(id, updatedFeesInfo);

			if (updatedFeesInfoRecord != null) {
				return ResponseEntity.ok(updatedFeesInfoRecord);
			} else {
				return ResponseEntity.notFound().build();
			}
		}



}
