package org.dnyanyog.controller;

import org.dnyanyog.dto.request.LoginRequest;
import org.dnyanyog.dto.request.SignUpRequest;
import org.dnyanyog.dto.request.SignUpUserRequest;
import org.dnyanyog.dto.response.LoginResponse;
import org.dnyanyog.dto.response.SignUpResponse;
import org.dnyanyog.dto.response.SignUpUserResponse;
import org.dnyanyog.service.StudentService;
import org.dnyanyog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SignUpController {
	@Autowired
	StudentService studentService;
	@Autowired
	UserService userService;
	
	
	@PostMapping(path="api/school/signup", produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE},consumes= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<SignUpResponse> signUp(@RequestBody SignUpRequest request){
		return studentService.saveData(request);
	}

	@PostMapping(path="api/school/signupUser", produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE},consumes= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<SignUpUserResponse> signUpUser(@RequestBody SignUpUserRequest request){
		return userService.registerUser(request);
	}
	@PostMapping(path="api/school/loginUser", produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE},consumes= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<LoginResponse> loginUser(@RequestBody LoginRequest request){
		return userService.login(request);
	}
	

}
