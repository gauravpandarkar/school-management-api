package org.dnyanyog.controller;

import java.time.LocalDate;
import java.util.List;

import org.dnyanyog.entity.Notice;
import org.dnyanyog.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NoticeController {
	@Autowired
	 NoticeService noticeService;
		
		
		@PostMapping(path="Notice/api/create")
		public Notice createNotice(@RequestBody Notice notice) {
			
			return noticeService.createNotice(notice);
		}
		

		
		@GetMapping(path="Notice/api/Notices/{noticeDate}")
	    public ResponseEntity<List<Notice>> getNoticesByDate(@PathVariable("noticeDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate noticeDate) {
	        List<Notice> notices = noticeService.getNoticesByDate(noticeDate);
	        if (!notices.isEmpty()) {
	            return ResponseEntity.ok(notices);
	        } else {
	            return ResponseEntity.notFound().build();
	        }
	    }
		 @DeleteMapping("/Notice/api/noticeById/{id}")
		    public ResponseEntity<?> deleteNoticeById(@PathVariable("id") long id) {
		        boolean deleted = noticeService.deleteNoticeById(id);
		        if (deleted) {
		            return ResponseEntity.ok().build();
		        } else {
		            return ResponseEntity.notFound().build();
		        }
		    }
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 @PutMapping("/Notice/api/update/{id}")
		    public ResponseEntity<Notice> updateNotice(@PathVariable long id, @RequestBody Notice updatedNotice) {
		        Notice updatedNoticeRecord = noticeService.updateNotice(id, updatedNotice);

		        if (updatedNoticeRecord != null) {
		            return ResponseEntity.ok(updatedNoticeRecord);
		        } else {
		            return ResponseEntity.notFound().build();
		        }
		    }

}
