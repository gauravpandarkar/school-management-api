package org.dnyanyog.controller;

import java.util.List;

import org.dnyanyog.dto.request.FeesRequest;
import org.dnyanyog.dto.request.LeaveRequest;
import org.dnyanyog.dto.response.FeesResponse;
import org.dnyanyog.dto.response.LeaveResponse;
import org.dnyanyog.entity.FeesInfo;
import org.dnyanyog.entity.LeaveApplication;
import org.dnyanyog.entity.LeaveStatus;
import org.dnyanyog.service.FeesService;
import org.dnyanyog.service.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LeaveController {
	@Autowired
	LeaveService leaveApplicationService;

	@Autowired
	LeaveApplication leaveApplication;

	@PostMapping(path = "Leave/api/request")
	public ResponseEntity<LeaveResponse> applyForLeave(@RequestBody LeaveRequest leaveApplicationRequest) {
		return leaveApplicationService.applyforLeave(leaveApplicationRequest);
	}

	@GetMapping(path = "Leave/api/show/{status}")
	public List<LeaveApplication> getLeaveRequestsByStatus(@PathVariable String status) {
		LeaveStatus leaveStatus = LeaveStatus.valueOf(status.toUpperCase());
		return leaveApplicationService.getLeaveRequestsByStatus(status);
	}

	
	/*
	@GetMapping(path="/Leave/api/v1/request/{grnNo}")
	public ResponseEntity<LeaveApplication> getLeaveRequestByGRN(@PathVariable Long grnNo) {
	    LeaveApplication leaveRequest = leaveApplicationService.getLeaveRequestByGRN(grnNo);

	    if (leaveRequest != null) {
	        return ResponseEntity.ok(leaveRequest);
	    } else {
	        return ResponseEntity.notFound().build();
	    }
	}
	*/
	@GetMapping(path = "/Leave/api/v1/request/{grnNo}")
	public ResponseEntity<List<LeaveApplication>> getLeaveRequestsByGrnNo(@PathVariable Long grnNo) {
		List<LeaveApplication> leaveRequests = leaveApplicationService.getLeaveRequestsByGrnNo(grnNo);

		if (!leaveRequests.isEmpty()) {
			return ResponseEntity.ok(leaveRequests);
		} else {
			return ResponseEntity.notFound().build();
		}
	}


	
	
	
	
	
	@PutMapping("/Leave/api/v1/{requestId}/{status}")
	public ResponseEntity<String> updateLeaveRequestStatus(@PathVariable Long requestId,
	                                                       @PathVariable String status) {
	    LeaveStatus leaveStatus;
	    try {
	        leaveStatus = LeaveStatus.valueOf(status.toUpperCase());
	    } catch (IllegalArgumentException e) {
	        return ResponseEntity.badRequest().body("Invalid status provided");
	    }

	    if (leaveStatus == LeaveStatus.APPROVED) {
	        boolean isApproved = leaveApplicationService.approveLeaveRequest(requestId);

	        if (isApproved) {
	            return ResponseEntity.ok("Leave request approved successfully");
	        } else {
	            return ResponseEntity.notFound().build();
	        }
	    } else if (leaveStatus == LeaveStatus.REJECTED) {
	        boolean isRejected = leaveApplicationService.rejectLeaveRequest(requestId);

	        if (isRejected) {
	            return ResponseEntity.ok("Leave request rejected successfully");
	        } else {
	            return ResponseEntity.notFound().build();
	        }
	    } else {
	        return ResponseEntity.badRequest().body("Invalid status provided");
	    }
	}
	@PutMapping("/Leave/api/update/{id}")
	public ResponseEntity<LeaveApplication> updateLeave(@PathVariable Long id,
			@RequestBody LeaveApplication updatedLeave) {
		LeaveApplication updatedLeaveRecord = leaveApplicationService.updateLeave(id, updatedLeave);

		if (updatedLeaveRecord != null) {
			return ResponseEntity.ok(updatedLeaveRecord);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	@DeleteMapping("/Leave/api/Delete/{id}")
	public ResponseEntity<String> deleteLeave(@PathVariable Long id) {
		boolean Delete = leaveApplicationService.deleteLeaveById(id);

		if (Delete) {
			return ResponseEntity.ok("Leave with ID " + id + " deleted successfully");

		} else {
			return ResponseEntity.notFound().build();
		}
	}





}
