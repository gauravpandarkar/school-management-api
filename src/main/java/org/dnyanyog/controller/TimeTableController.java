package org.dnyanyog.controller;

import java.util.List;

import org.dnyanyog.dto.request.TimeTableRequest;
import org.dnyanyog.dto.response.TimeTableResponse;
import org.dnyanyog.entity.TimeTable;
import org.dnyanyog.repository.TimeTableRepository;
import org.dnyanyog.service.TimeTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TimeTableController {
	

	

		@Autowired
	     TimeTableService timetableService;

	    @Autowired
	    TimeTableRepository repository;

	    @GetMapping
	    public List<TimeTable> getAllTimetableEntries() {
	        return timetableService.getAllTimetableEntries();
	    }
	    
	    

	    @GetMapping(path="Timetable/api/{className}")
	    public List<TimeTable> getTimetableEntriesByClass(@PathVariable String className) {
	        return timetableService.getTimetableEntriesByClass(className);
	    }
	    
	    
	    

	    @PostMapping(path="Timetable/api/add")
	    public ResponseEntity<TimeTableResponse> createTimetable(@RequestBody TimeTableRequest request) {
	     
	        return timetableService.createTimetable(request);
	    }
	    
	    
	    
	    @DeleteMapping(path="/Timetable/Api/delete/{className}")
	    public ResponseEntity<String> deleteTimetableByClassName(@PathVariable String className) {
	        try {
	           
	            boolean deleted = timetableService.deleteTimetableByClassName(className);

	            if (deleted) {
	                return ResponseEntity.ok("Timetable entry with className " + className + " deleted successfully.");
	            } else {
	                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Timetable entry with className " + className + " not found.");
	            }
	        } catch (Exception e) {
	            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred while deleting timetable entry.");
	        }
	    }
	    
	    
	    
	    
	    
	    @PutMapping("/Timetable/Api/update/{id}")
	    public ResponseEntity<TimeTable> updateTimetable(@PathVariable Long id, @RequestBody TimeTable updatedTimetable) {
	        TimeTable updatedEntity = timetableService.updateTimetable(id, updatedTimetable);
	        if (updatedEntity != null) {
	            return new ResponseEntity<>(updatedEntity, HttpStatus.OK);
	        } else {
	            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	        }
	    }
	




}
