package org.dnyanyog.controller;

import java.util.List;

import org.dnyanyog.entity.Attendance;
import org.dnyanyog.repository.AttendanceRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/persons")
public class PersonController {
	
	private final AttendanceRepository attendanceRepository;

    public PersonController(AttendanceRepository attendanceRepository) {
        this.attendanceRepository = attendanceRepository;
    }

    @GetMapping
    public List<Attendance> getAllPersons() {
        return attendanceRepository.findAll();
    }

}
