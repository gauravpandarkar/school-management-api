package org.dnyanyog.controller;

import java.util.List;

import org.dnyanyog.dto.request.UpdatePasswordRequest;
import org.dnyanyog.dto.response.SignUpUserResponse;
import org.dnyanyog.dto.response.SignUpUserResponseNew;
import org.dnyanyog.entity.Notice;
import org.dnyanyog.entity.Users;
import org.dnyanyog.service.UserFetchedService;
import org.dnyanyog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@Autowired
	UserFetchedService userService;
	@Autowired
	UserService service;
	@GetMapping(path="User/api/{email}")
	public SignUpUserResponse getUserByEmail(@PathVariable String email){
      //  List<Users> users = userService.getUserByEmail(email);
         return userService.getUserByEmail(email);
	}
	
//	@GetMapping(path="User/api/users/search")
//	public SignUpUserResponseNew getUsers() {
//		return userService.getUser();
//	}
	 @PostMapping("/updatePassword")
	    public ResponseEntity<String> updatePasswordByEmail(@RequestBody UpdatePasswordRequest request) {
	        boolean updated = service.updatePasswordByEmail(request.getEmail(), request.getNewPassword());
	        if (updated) {
	            return ResponseEntity.ok("Password updated successfully");
	        } else {
	            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found with the provided email");
	        }
	    }

}
