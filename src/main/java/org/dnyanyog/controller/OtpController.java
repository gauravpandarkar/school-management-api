package org.dnyanyog.controller;

import org.dnyanyog.dto.request.EmailRequest;
import org.dnyanyog.dto.response.OtpResponse;
import org.dnyanyog.service.EmailService;
import org.dnyanyog.service.OtpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class OtpController {
	@Autowired
    private EmailService emailService;

    @Autowired
    private OtpService otpService;

    @RequestMapping(value="/sendotp",method=RequestMethod.POST)
	public ResponseEntity<OtpResponse> sendOtp(@RequestBody EmailRequest request){
		
		
		System.out.println(request);
	boolean result=	this.otpService.sendOtp(request.getSubject(), request.getMessage(), request.getTo());
		OtpResponse otpResponse=new OtpResponse();
	
	if(result) {
		String otp=otpService.getOtpValue();	
//		System.out.println("hello"+otp);
//		return ResponseEntity.ok(otpService.getOtpValue());
		otpResponse.setOtp(otp);
		
		
	}else {
		//return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body( "Email not sent ...");

		otpResponse.setOtp(null);
	}
	return ResponseEntity.ok(otpResponse);
		
	}

}
