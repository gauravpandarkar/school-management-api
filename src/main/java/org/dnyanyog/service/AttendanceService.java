package org.dnyanyog.service;

import org.dnyanyog.dto.request.AttendenceRequest;
import org.dnyanyog.dto.response.AttendanceResponse;
import org.dnyanyog.entity.Attendance;
import org.dnyanyog.repository.AttendanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AttendanceService {
	@Autowired
	AttendanceResponse attendanceResponse;
	@Autowired
	AttendenceRequest attendanceRequest;
	@Autowired
	Attendance attendance;
	@Autowired
	AttendanceRepository attendanceRepository;

	public ResponseEntity<AttendanceResponse> saveData(AttendenceRequest request) {
		attendanceResponse = new AttendanceResponse();

		attendance = new Attendance();
		if (null != attendanceRepository.findByCurDate(request.getCurDate())
				&& null != attendanceRepository.findByRollNo(request.getRollNo())) {
			return getConflictAttendanceResponse();
		}
		attendance.setName(request.getName());
		attendance.setCurDate(request.getCurDate());
		attendance.setAttendance(request.getAttendance());
		attendance = attendanceRepository.save(attendance);
		attendanceResponse.setStatus("sucess");
		attendanceResponse.setMessage("Attendance done successfully ");
		attendanceResponse.setName(attendance.getName());
		attendanceResponse.setAttendance(attendance.getAttendance());

		return ResponseEntity.status(HttpStatus.CREATED).body(attendanceResponse);
	}

	private ResponseEntity<AttendanceResponse> getConflictAttendanceResponse() {
		AttendanceResponse response = new AttendanceResponse();
		response.setStatus("error");
		response.setMessage("Attendance already exists");
		response.setAttendance("Absent");

		return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
	}

}
