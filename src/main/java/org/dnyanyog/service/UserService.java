package org.dnyanyog.service;

import java.util.Optional;

import org.dnyanyog.dto.request.LoginRequest;
import org.dnyanyog.dto.request.SignUpUserRequest;
import org.dnyanyog.dto.response.LoginResponse;
import org.dnyanyog.dto.response.SignUpResponse;
import org.dnyanyog.dto.response.SignUpUserResponse;
import org.dnyanyog.dto.response.UserData;
import org.dnyanyog.dto.response.UserFails;
import org.dnyanyog.entity.Users;
import org.dnyanyog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired
	LoginResponse loginResponse;
	@Autowired
	UserData userData;
	@Autowired
	UserFails userFails;
	@Autowired 
	UserRepository userRepository;
	@Autowired
	Users user;
	@Autowired
	SignUpUserResponse userResponse;
	public ResponseEntity<LoginResponse> login(LoginRequest request){
		loginResponse=new LoginResponse();
		loginResponse.setData(new UserData());
		if(null!=userRepository.findByMobileNo(request.getMobileNo()) && null!=userRepository.findByPassword(request.getPasscode())) {
			user=new Users();
			user.setMobileNo(request.getMobileNo());
			user.setPassword(request.getPasscode());
			//user.setUserRole("Principal");
			
			user=userRepository.findByMobileNo(request.getMobileNo());
			user=userRepository.findByPassword(request.getPasscode());
			//user=userRepository.findByUserRole("Principal");
			
			
			loginResponse.setStatus("success");
			loginResponse.setMessage("Validation Sucessfull");
			loginResponse.getData().setMobileNo(user.getMobileNo());
			
			loginResponse.getData().setUserRole(user.getUserRole());
			loginResponse.getData().setEmail(user.getEmail());
			loginResponse.getData().setGrnNo(user.getId());
			loginResponse.setErrors(null);
			return ResponseEntity.status(HttpStatus.CREATED).body(loginResponse);
		}
		
		loginResponse.setStatus("error");
		loginResponse.setMessage("Mobile number and password are invalid");
		loginResponse.setData(null);
		loginResponse.getErrors().setField("mobile number");
		loginResponse.getErrors().setField("Mobile number is required");
		loginResponse.getErrors().setField("password");
		loginResponse.getErrors().setField("Password is required");
		
		return ResponseEntity.status(HttpStatus.CONFLICT).body(loginResponse);
		
	}
	public ResponseEntity<SignUpUserResponse> registerUser(SignUpUserRequest request){
		userResponse=new SignUpUserResponse();
		userResponse.setData(new UserData());
		user=new Users();
		if(null!=userRepository.findByMobileNo(request.getMobileNo())) {
			return getConflictSignUpResponse();
		}
		if(null!=userRepository.findByEmail(request.getEmail())) {
			return getConflictSignUpResponse();
		}
		
		user.setMobileNo(request.getMobileNo());
		user.setPassword(request.getPasscode());
		user.setUserRole(request.getUserRole());
		user.setEmail(request.getEmail());
		
		user=userRepository.saveAndFlush(user);
		
		userResponse.setStatus("Success");
		userResponse.setMessage("User created successfully");
		userResponse.getData().setMobileNo(user.getMobileNo());
		userResponse.getData().setUserRole(user.getUserRole());
		userResponse.getData().setGrnNo(user.getId());
		userResponse.getData().setEmail(user.getEmail());
		return ResponseEntity.status(HttpStatus.CREATED).body(userResponse); 		
	}
	private ResponseEntity<SignUpUserResponse> getConflictSignUpResponse() {
		SignUpResponse response = new SignUpResponse();
		userResponse.setStatus("error");
		userResponse.setMessage("Mobile No already register");
		userResponse.setData(null);

		return ResponseEntity.status(HttpStatus.CONFLICT).body(userResponse);
	}
	
	
	
	
//	 public boolean updatePasswordByEmail(String email, String newPassword) {
//	        Optional<Users> optionalUsers = userRepository.findByEmail(email);
//	        if (optionalUsers.isPresent()) {
//	            Users user = optionalUsers.get();
//	            user.setPassword(newPassword);
//	            userRepository.save(user);
//	            return true;
//	        }
//	        return false;
//	    }
//	
	
	  public boolean updatePasswordByEmail(String email, String newPassword) {
	        Users user = userRepository.findByEmail(email);
	        if (user != null) {
	            user.setPassword(newPassword);
	            userRepository.save(user);
	            return true;
	        }
	        return false;
	    }

}
