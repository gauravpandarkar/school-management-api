package org.dnyanyog.service;

import org.dnyanyog.dto.request.SignUpRequest;
import org.dnyanyog.dto.response.SignUpResponse;
import org.dnyanyog.dto.response.StudentData;
import org.dnyanyog.entity.Student;
import org.dnyanyog.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class StudentService {
	@Autowired
	SignUpResponse signUpResponse;
	@Autowired
	Student student;
	@Autowired
	StudentRepository studentRepository;
	
	public ResponseEntity<SignUpResponse> saveData(SignUpRequest request){
		signUpResponse=new SignUpResponse();
		signUpResponse.setData(new StudentData());
		student=new Student();
		if(null!=studentRepository.findByParentMobileNo(request.getParentMobileNo())) { 
			return getConflictSignUpResponse();
		}
		student.setAddress(request.getAddress());
		student.setClassToApply(request.getClassToApply());
		student.setFatherFirstName(request.getFatherFirstName());
		student.setGender(request.getGender());
	    student.setLastName(request.getLastName());
	    student.setMotherFirstName(request.getMotherFirstName());
	    student.setParentMobileNo(request.getParentMobileNo());
	    student.setPreviousSchoolName(request.getPreviousSchoolName());
	    student.setStudentBirthDate(request.getStudentBirthDate());
	    student.setStudentEmail(request.getStudentEmail());
		student.setStudentFirstName(request.getStudentFirstName());
		student.setStudentMobileNo(request.getStudentMobileNo());
		
		student=studentRepository.save(student);
		
		signUpResponse.setStatus("success");
		signUpResponse.setMessage("add user successfully");
		
		signUpResponse.getData().setGrnNo(student.getGrnNo());
		signUpResponse.getData().setAddress(student.getAddress());
		signUpResponse.getData().setClassToApply(student.getClassToApply());
		signUpResponse.getData().setFatherFirstName(student.getFatherFirstName());
		signUpResponse.getData().setGender(student.getGender());
		signUpResponse.getData().setLastName(student.getLastName());
		signUpResponse.getData().setMotherFirstName(student.getMotherFirstName());
		signUpResponse.getData().setParentMobileNo(student.getParentMobileNo());
		signUpResponse.getData().setPreviousSchoolName(student.getPreviousSchoolName());
		signUpResponse.getData().setStudentBirthDate(student.getStudentBirthDate());
		signUpResponse.getData().setStudentEmail(student.getStudentEmail());
		signUpResponse.getData().setStudentFirstName(student.getStudentFirstName());
		signUpResponse.getData().setStudentMobileNo(student.getStudentMobileNo());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(signUpResponse);       

	}
	private ResponseEntity<SignUpResponse> getConflictSignUpResponse() {
		SignUpResponse response = new SignUpResponse();
		response.setStatus("error");
		response.setMessage("Email or mobile number already registered");
		response.setData(null);

		return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
	}

}
