package org.dnyanyog.service;

import java.util.List;
import java.util.Optional;

import org.dnyanyog.dto.request.TimeTableRequest;
import org.dnyanyog.dto.response.TimeTableData;
import org.dnyanyog.dto.response.TimeTableResponse;
import org.dnyanyog.entity.TimeTable;
import org.dnyanyog.repository.TimeTableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class TimeTableService {
	@Autowired
	TimeTableRepository repository;

	@Autowired
	TimeTableResponse response;

	@Autowired
	TimeTable timetable;

	@Autowired
	TimeTableData timetableData;

	public List<TimeTable> getAllTimetableEntries() {
		return repository.findAll();
	}

	public List<TimeTable> getTimetableEntriesByClass(String className) {
		return repository.findByClassName(className);
	}

	public ResponseEntity<TimeTableResponse> createTimetable(@RequestBody TimeTableRequest request) {

		response = new TimeTableResponse();
		response.setData(new TimeTableData());

		timetable = new TimeTable();

		timetable.setTimeSlot(request.getTimeslot());
		timetable.setClassName(request.getClassname());
		timetable.setMonDay(request.getMonDay());
		timetable.setTuesDay(request.getTuesDay());
		timetable.setWednesDay(request.getWednesDay());
		timetable.setThursDay(request.getThursDay());
		timetable.setFriDay(request.getFriDay());
		timetable.setSaturDay(request.getSaturDay());

		timetable = repository.save(timetable);

		response.setStatus("Success");
		response.setMessage("TimeTable Entry Added Suceesfully");
		response.getData().setId(timetable.getId());
		response.getData().setTimeslot(timetable.getTimeSlot());
		response.getData().setClassname(timetable.getClassName());
		response.getData().setMonDay(timetable.getMonDay());
		response.getData().setTuesDay(timetable.getTuesDay());
		response.getData().setWednesDay(timetable.getWednesDay());
		response.getData().setThursDay(timetable.getThursDay());
		response.getData().setFriDay(timetable.getFriDay());
		response.getData().setSaturDay(timetable.getSaturDay());

		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	public boolean deleteTimetableByClassName(String className) {
		try {

			List<TimeTable> timetable = repository.findByClassName(className);

			if (timetable != null) {

				repository.deleteAll(timetable);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	 public TimeTable updateTimetable(Long id, TimeTable updatedTimetable) {
	        Optional<TimeTable> existingTimetable = repository.findById(id);
	        if (existingTimetable.isPresent()) {
	            TimeTable timetableToUpdate = existingTimetable.get();
	            timetableToUpdate.setTimeSlot(updatedTimetable.getTimeSlot());
	            timetableToUpdate.setClassName(updatedTimetable.getClassName());
	            timetableToUpdate.setMonDay(updatedTimetable.getMonDay());
	            timetableToUpdate.setTuesDay(updatedTimetable.getTuesDay());
	            timetableToUpdate.setWednesDay(updatedTimetable.getWednesDay());
	            timetableToUpdate.setThursDay(updatedTimetable.getThursDay());
	            timetableToUpdate.setFriDay(updatedTimetable.getFriDay());
	            timetableToUpdate.setSaturDay(updatedTimetable.getSaturDay());

	            return repository.save(timetableToUpdate);
	        }
	        return null;
	    }


}
