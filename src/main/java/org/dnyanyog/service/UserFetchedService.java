package org.dnyanyog.service;

import java.util.ArrayList;
import java.util.List;

import org.dnyanyog.dto.response.SignUpUserResponse;
import org.dnyanyog.dto.response.SignUpUserResponseNew;
import org.dnyanyog.dto.response.UserData;
import org.dnyanyog.entity.Users;
import org.dnyanyog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserFetchedService {
	@Autowired
	UserRepository userRepository;
	@Autowired
	Users user;
	@Autowired
	SignUpUserResponse response;
	@Autowired
	UserData data;
//	public List<Users> getUserByEmail1(String email){
//		return userRepository.findByEmail(email);
//		
//	}
	public SignUpUserResponse getUserByEmail(String email) {
		user=userRepository.findByEmail(email);
		response.getData().setEmail(user.getEmail());		
		return response;
	}
	
	
	
	
	
	
	
	
	
	
	
	
//	public SignUpUserResponseNew getUser() {
//		List<Users> userList=userRepository.findAll();
//		List<UserData> getUserList=new ArrayList<>();
//		for(Users user:userList) {
////			response.getData().setEmail(user.getEmail());
////			response.getData().setMobileNo(user.getMobileNo());
////			response.getData().setUserRole(user.getUserRole());
////			response.getData().setGrnNo(user.getId());
//		   UserData data=new UserData();
//			data.setEmail(user.getEmail());
//			data.setGrnNo(user.getId());
//			data.setMobileNo(user.getMobileNo());
//			data.setUserRole(user.getUserRole());
//			getUserList.add(data);
//			
//		}
//		response.setData(getUserList);
//		return response;
//	}
	

}
