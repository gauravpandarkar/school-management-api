package org.dnyanyog.repository;

import java.time.LocalDate;
import java.util.List;

import org.dnyanyog.entity.Notice;
import org.dnyanyog.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

	Users findByMobileNo(String mobileNo);

	Users findByPassword(String password);

	Users findByUserRole(String userRole);
	Users findByEmail(String email);
	// List<Users> findByEmail(String email);
}
