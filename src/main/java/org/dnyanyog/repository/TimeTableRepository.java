package org.dnyanyog.repository;

import java.util.List;

import org.dnyanyog.entity.TimeTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimeTableRepository extends JpaRepository<TimeTable,Long> {
	List<TimeTable> findByClassName(String className);
	

}
