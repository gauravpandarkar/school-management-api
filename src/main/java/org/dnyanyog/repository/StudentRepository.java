package org.dnyanyog.repository;

import org.dnyanyog.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {
	Student findByStudentMobileNo(String studentMobileNo);
	Student findByParentMobileNo(String parentMobileNo);
	Student findByStudentEmail(String email);
	Student findBygrnNo(long studentId);
}
