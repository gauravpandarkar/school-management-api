package org.dnyanyog.repository;

import java.util.List;
import java.util.Optional;

import org.dnyanyog.entity.DiaryInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiaryRepository extends JpaRepository <DiaryInfo,Long> {

	List<DiaryInfo> findByGrnNO(Long grnNO);
	
	Optional<DiaryInfo> findById(Long id);
}
