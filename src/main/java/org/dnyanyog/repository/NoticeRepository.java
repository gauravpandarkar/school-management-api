package org.dnyanyog.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.dnyanyog.entity.Notice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoticeRepository extends JpaRepository<Notice,Long>{
	 Optional<Notice> findById(long id);
	  
	  List<Notice> findByNoticeDate(LocalDate noticeDate);
	  
	  void deleteById(long id);
	  
	  void deleteByNoticeDate(LocalDate noticeDate);
	  


}
